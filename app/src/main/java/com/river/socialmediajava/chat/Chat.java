package com.river.socialmediajava.chat;

public class Chat {
    String message, receiver, sender, lastTime;
    boolean isSeen;

    public Chat() {
    }

    public Chat(String message, String receiver, String sender, String lastTime, boolean isSeen) {
        this.message = message;
        this.receiver = receiver;
        this.sender = sender;
        this.lastTime = lastTime;
        this.isSeen = isSeen;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public boolean isSeen() {
        return isSeen;
    }

    public void setSeen(boolean seen) {
        isSeen = seen;
    }
}
