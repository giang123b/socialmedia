package com.river.socialmediajava.chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.river.socialmediajava.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterUser extends  RecyclerView.Adapter<AdapterUser.MyViewHolder> {

    Context context;
    List<User> userList;

    public AdapterUser(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_user, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String hisUID = userList.get(position).getUid();
        String image = userList.get(position).getImage();
        String name = userList.get(position).getName();
        String email = userList.get(position).getEmail();

        holder.textViewName.setText("Name: " + name);
        holder.textViewEmail.setText("Email: " + email);

        try{
            Picasso.get().load(image)
                    .placeholder(R.drawable.anh)
                    .into(holder.imageViewAvatar);
        }catch (Exception e){

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "" + name + " + " + hisUID, Toast.LENGTH_SHORT). show();
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("receiverID", hisUID);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageViewAvatar;
        TextView textViewName, textViewEmail;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewAvatar = itemView.findViewById(R.id.imageViewAvatar);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewEmail = itemView.findViewById(R.id.textViewEmail);
        }
    }
}
