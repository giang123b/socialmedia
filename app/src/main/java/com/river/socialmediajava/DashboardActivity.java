package com.river.socialmediajava;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.river.socialmediajava.chat.UserFragment;
import com.river.socialmediajava.no_internet.NetworkChangeReceiver;
import com.river.socialmediajava.notification.FirebaseService;
import com.river.socialmediajava.notification.Token;
import com.river.socialmediajava.profile.ProfileFragment;
import com.river.socialmediajava.welcome.MainActivity;

public class DashboardActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
//    TextView textViewInformation;
    ActionBar actionBar;
    String mUID;

    private BroadcastReceiver mNetworkReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        setActionBar();

        UserFragment fragment2 = new UserFragment();
        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.content, fragment2, "");
        fragmentTransaction2.commit();

        firebaseAuth = FirebaseAuth.getInstance();

        checkUserStatus();

        FirebaseService firebaseService = new FirebaseService();
        updateToken(firebaseService.getTokenDay(this));
        Log.e("Token DashboardActivity", "day la token: " + firebaseService.getTokenDay(this));
//        updateToken();

        BottomNavigationView navigationView = findViewById(R.id.navigationBottom);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
//                    case R.id.nav_home:
//                        actionBar.setTitle("Home");
//                        HomeFragment fragment = new HomeFragment();
//                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                        fragmentTransaction.replace(R.id.content, fragment, "");
//                        fragmentTransaction.commit();
//                        return true;

                    case R.id.nav_profile:
                        actionBar.setTitle("Profile");
                        ProfileFragment fragment1 = new ProfileFragment();
                        FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction1.replace(R.id.content, fragment1, "");
                        fragmentTransaction1.commit();

                        return true;

                    case R.id.nav_user:
                        actionBar.setTitle("Chats");
                        UserFragment fragment2 = new UserFragment();
                        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction2.replace(R.id.content, fragment2, "");
                        fragmentTransaction2.commit();

                        return true;

//                    case R.id.nav_chat:
//                        ChatListFragment fragment3 = new ChatListFragment();
//                        FragmentTransaction fragmentTransaction3 = getSupportFragmentManager().beginTransaction();
//                        fragmentTransaction3.replace(R.id.content, fragment3, "");
//                        fragmentTransaction3.commit();
//
//                        actionBar.setTitle("Chat list");
//                        return true;
                }

                return false;
            }
        });

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();
//
//        if (!mNetworkReceiver.isOnline(this)){
//            startActivity(new Intent(this, NoInternetActivity.class));
//        }

    }


    @Override
    protected void onResume() {
        checkUserStatus();
        super.onResume();
    }

    public void updateToken(String token){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token mToken = new Token(token);
        databaseReference.child(mUID).setValue(mToken);
    }

    private void setActionBar() {
        actionBar = getSupportActionBar();
        actionBar.setTitle("Chats");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void checkUserStatus() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            mUID = user.getUid();
            SharedPreferences sharedPreferences = getSharedPreferences("SP_USER", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Current_USERID", mUID);
            editor.apply();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkUserStatus();
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }
}








