package com.river.socialmediajava.notification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAm2HjaRU:APA91bHYfOJPkbG8BA2Swj2Z_UEPjA2lXPYE9uy-LUX6231a9ZKJ6_xGD9J1a9M218eOaXOvPdT_9DTd2Ox-7btRcxjlktASgg0FgRBoyBFT2VKTMHEZjPCD1UdR1d57wFI_cNZ4N1Wc"
    })
    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);
}
