package com.river.socialmediajava.welcome;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.river.socialmediajava.DashboardActivity;
import com.river.socialmediajava.login.LoginActivity;
import com.river.socialmediajava.R;

public class MainActivity extends AppCompatActivity {

    Button btnLogin, btnRegister;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//            @Override
//            public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                if(!task.isSuccessful()) {
//                    return;
//                }
//                // Get new Instance ID token
//                String token = task.getResult().getToken();
//
//                Log.e( "deviceToken: ", token);
//            }
//        });

        firebaseAuth = FirebaseAuth.getInstance();

//        ActionBar ac
        getSupportActionBar().hide();

        findView();

        setClick();
    }

    private void setClick() {

//        btnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
//            }
//        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });
    }

    private void findView() {

        btnLogin = findViewById(R.id.buttonLogin);
//        btnRegister = findViewById(R.id.register_btn);
    }

    @Override
    protected void onStart() {
        checkUserStatus();

        super.onStart();
    }

    private void checkUserStatus(){
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null){
            startActivity(new Intent(this, DashboardActivity.class));
            finish();
        }
    }
}